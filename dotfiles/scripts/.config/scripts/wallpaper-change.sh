#!/bin/bash

# Get the current hour (0-23)
hour=$(date +"%H")

# Define the time range for night mode
night_start=22  # 10 PM
night_end=6     # 6 AM

# it is not possible to use user environment variables
config_home="/home/marakuja/.config"

# DISPLAY:=0 was obtained by running env | grep -i display
# Set the wallpaper based on the time
if [ $hour -ge $night_start ] || [ $hour -lt $night_end ]; then
    DISPLAY=:0 feh --bg-scale ${config_home}/wallpapers/wallpaper-night
else
    DISPLAY=:0 feh --bg-scale ${config_home}/wallpapers/wallpaper-day
fi


#!/bin/bash

# https://chatgpt.com/c/6767e637-3ff0-800b-b449-6732fe015a00
# Check if at least two arguments are provided
if [ "$#" -lt 2 ]; then
    echo "Usage: $0 <monitor-name> <on|off> [direction]"
    exit 1
fi

# Extract parameters
MONITOR_NAME=$1
ACTION=$2
DIRECTION=${3:-right}  # Default direction is right

# Check if action is 'on'
if [ "$ACTION" == "on" ]; then
    # Run xrandr command to turn on the monitor in the specified or default direction
    xrandr --output "$MONITOR_NAME" --auto --${DIRECTION}-of eDP-1
    if [ $? -eq 0 ]; then
        echo "Monitor '$MONITOR_NAME' turned on to the $DIRECTION of eDP-1."
    else
        echo "Failed to configure monitor '$MONITOR_NAME'."
    fi
elif [ "$ACTION" == "off" ]; then
    # Run xrandr command to turn off the monitor
    xrandr --output "$MONITOR_NAME" --off
    if [ $? -eq 0 ]; then
        echo "Monitor '$MONITOR_NAME' turned off."
    else
        echo "Failed to turn off monitor '$MONITOR_NAME'."
    fi
else
    echo "Error: Invalid action '$ACTION'. Use 'on' or 'off'."
    exit 1
fi

local colors = {0xf8f8f2, 0x8be9fd, 0X50fa7b, 0X9AEDFE, 0Xff5555, 0XFF80BF, 0X808080, 0XFF9580}
local iClIndex = 1

local aTools = {
"ACTION_TOOL_PEN",
"ACTION_RULER",
"ACTION_TOOL_DRAW_RECT",
"ACTION_TOOL_DRAW_ELLIPSE",
"ACTION_TOOL_DRAW_ARROW",
"ACTION_TOOL_DRAW_SPLINE"
}
local iTlIndex = 1

local thickness = 1
local lineIndex = 1

function initUi()

-- COLORS -------------------------------------------------------------------------------------------------------
  app.registerUi({["menu"] = "Color1", ["callback"] = "Color1", ["accelerator"] = "<Alt>1"});
  app.registerUi({["menu"] = "Color2", ["callback"] = "Color2", ["accelerator"] = "<Alt>2"});
  app.registerUi({["menu"] = "Color3", ["callback"] = "Color3", ["accelerator"] = "<Alt>3"});
  app.registerUi({["menu"] = "Color4", ["callback"] = "Color4", ["accelerator"] = "<Alt>4"});
  app.registerUi({["menu"] = "Color5", ["callback"] = "Color5", ["accelerator"] = "<Alt>5"});
  app.registerUi({["menu"] = "Color6", ["callback"] = "Color6", ["accelerator"] = "<Alt>6"});
  app.registerUi({["menu"] = "Color7", ["callback"] = "Color7", ["accelerator"] = "<Alt>7"});
  app.registerUi({["menu"] = "Color8", ["callback"] = "Color8", ["accelerator"] = "<Alt>8"});

-- THICKNESS ----------------------------------------------------------------------------------------------------
  app.registerUi({["menu"] = "ChangeThickness", ["callback"] = "ChangeThiccness", ["accelerator"] = "t"});

-- PAGES -----------------------------------------------------------------------------------------------------
  app.registerUi({["menu"] = "New page", ["callback"] = "new_page", ["accelerator"] = "n"});
  app.registerUi({["menu"] = "Delete page", ["callback"] = "delete_page", ["accelerator"] = "r"});

-- TOOLS
  app.registerUi({["menu"] = "Highlighter", ["callback"] = "highlighter", ["accelerator"] = "d"})
  app.registerUi({["menu"] = "Pen", ["callback"] = "pen", ["accelerator"] = "f"})
  app.registerUi({["menu"] = "Line", ["callback"] = "line", ["accelerator"] = "s"})
end

function line()
  lineIndex = lineIndex % 2 + 1
  if lineIndex == 1 then
    app.uiAction({["action"] = "ACTION_RULER", ["enabled"]=false})
  else
    app.uiAction({["action"] = "ACTION_RULER", ["enabled"]=true})
  end
end

function highlighter()
  app.uiAction({["action"] = "ACTION_TOOL_HIGHLIGHTER"})
end

function pen()
  app.uiAction({["action"] = "ACTION_TOOL_PEN"})
end

function new_page()
  app.uiAction({["action"] = "ACTION_NEW_PAGE_AFTER"})
end

function delete_page()
  app.uiAction({["action"] = "ACTION_DELETE_PAGE"})
end

function Selection()
  app.uiAction({["action"] = "ACTION_TOOL_SELECT_RECT"})
end

function move_hand()
  app.uiAction({["action"] = "ACTION_TOOL_HAND"})
end

function ChangeThiccness()
  local thickness_types = {"ACTION_SIZE_FINE", "ACTION_SIZE_MEDIUM", "ACTION_SIZE_THICK", "ACTION_SIZE_VERY_THICK"}

  -- go through each thickness type declared above
  thickness = thickness % #(thickness_types) + 1
  app.uiAction({["action"] = thickness_types[thickness]})
end

function change_color()
  app.uiAction({["action"] = "ACTION_TOOL_PEN"})
  app.changeToolColor({["color"] = colors[iClIndex], ["selection"] = true})
end

function change_tool()
  app.uiAction({["action"] = aTools[iTlIndex]})
end

function Color1()
  iClIndex = 1
  change_color()
end

function Color2()
  iClIndex = 2
  change_color()
end

function Color3()
  iClIndex = 3
  change_color()
end

function Color4()
  iClIndex = 4
  change_color()
end

function Color5()
  iClIndex = 5
  change_color()
end

function Color6()
  iClIndex = 6
  change_color()
end

function Color7()
  iClIndex = 7
  change_color()
end

function Color8()
  iClIndex = 8
  change_color()
end

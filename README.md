# Configuration after os installation

### Download
- bat
- helix
- iwd
- i3-wm, i3status, i3blocks, i3lock
- dmenu
- alacritty
- stow
- ttf-iosevka-nerd
- brightnessctl
- xournalpp
- qalculate-gtk
- mons (aur)
- spotify-launcher (aur)
- texlive-latexrecommended, texlive-mathscience, texlive-latex, texlive-latexextra
- texlive-fontsrecommended, texlive-binextra (for `latexmk`), texlive-langczechslovak
- texlive-plaingeneric (for `soul.sty`)
- zathura, zathura-pdf-mupdf
- feh (wallpaper)
- cron
- pulsemixer
- jetbrains-toolbox
- vesktop
- git
- polybar
- git-open (aur)
- nerd-fonts
- flameshot
- clang

### Beeping

turn off beeping sound on tab completion and more by [this](https://linuxconfig.org/turn-off-beep-bell-on-linux-terminal)

edit `/etc/inputrc` by uncommenting `#set bell-style none`
or write `blacklist pcspkr` to `/etc/modprobe.d/blacklist.conf`

### Set up i3
copy & modify .xinitrc [xinit](https://wiki.archlinux.org/title/Xinit)
```
cp /etc/X11/xinit/xinitrc ~/.xinitrc
```

### Set up natural scroll of touchpad
edit `/usr/share/X11/xorg.conf.d/30-touchpad` to this
```
Section "InputClass"
    Identifier "devname"
    Driver "libinput"
    Option "Tapping" "on"
    Option "NaturalScrolling" "Yes"
EndSection
```

### Disable natural scroll on mouse
create a new file in `/etc/X11/xorg.conf.d` directory and write this into it
```
Section "InputClass"
	Driver "libinput"
	Identifier "Rapoo Mouse"
	MatchProduct "<MOUSE-NAME>"
	Option "NaturalScrolling" "false"
EndSection
```
where `MOUSE-NAME` is given by the name of the device in `xinput list` list

for not lasting setting one can use `xinput set-prop <device-id> 340 [0,1]` to either disable
or enable natural scroll

to restrict device (f.e. wacom tablet) to only one monitor, one should use `xinput map-to-output <device-id> <monitor>`

### Set up network
Use systemd-networkd for DHCP. Create a file `/etc/systemd/network/25-wireless.network` and
edit it to this
```
[Match]
Name=wlan0

[Network]
DHCP=yes
IgnoreCarrierLoss=3s
```
enable iwd
```
sudo systemctl enable iwd
sudo systemcdtl start iwd
```
enable DHCP (systemd-networkd)
```
sudo systemctl enable systemd-networkd
sudo systemctl start systemd-networkd
```
enable DNS (systemd-resolved)
```
sudo systemctl enable systemd-resolved
sudo systemctl start systemd-networkd
```
disable NetworkManager
```
sudo systemctl disable NetworkManager
sudo systemctl stop NetworkManager
```
**Beware that when using pacman, mirrors can be outdated**
To change the mirrors, edit the file `/etc/pacman.d/mirrorlist`
### Set up eduroam
```
[Security]
EAP-Method=PEAP
EAP-PEAP-Phase2-Method=MSCHAPV2
EAP-PEAP-ServerDomainMask=radius.cvut.cz
EAP-PEAP-CACert=/etc/ssl/certs/USERTrust_RSA_Certification_Authority.pem
EAP-Identity=<CVUT EMAIL>
EAP-PEAP-Phase2-Identity=<CVUT EMAIL>
EAP-PEAP-Phase2-Password-Hash=<HASH>

[Settings]
AutoConnect=true
```
generate the hash using [iwd](https://wiki.archlinux.org/title/Iwd)
```
iconv -t utf16le | openssl md4 -provider legacy
```
put your password and EOF (hit Ctrl+D instead of pressing Enter)
and save the file to `/var/lib/iwd/eduroam.8021x`
and restart iwd
```
sudo systemctl restart iwd
```

### Set up keyboard
for temporary layout
```
setxkbmap -layout cz -variant coder
```
for lasting layout edit `/etc/X11/xorg.conf.d/00-keyboard.conf` to this
```
# Read and parsed by systemd-localed. It's probably wise not to edit this file
# manually too freely.
Section "InputClass"
        Identifier "system-keyboard"
        MatchIsKeyboard "on"
        Option "XkbLayout" "cz"
        Option "XkbVariant" "coder"
EndSection
```
more information [here](https://wiki.archlinux.org/title/Xorg/Keyboard_configuration)

### Set up ssh and gpg keys
- [ssh](https://docs.gitlab.com/ee/user/ssh.html)
- [gpg](https://docs.gitlab.com/ee/user/project/repository/signed_commits/gpg.html)

### Download and manage dotfiles
clone dotfiles
```
git clone git@gitlab.com:marakujaa/dotfiles.git
```
and stow them
```
# run in dotfiles/dotfiles directory
stow * -t ~ --adopt
```

### Set up git
#### Files user.*
create `user.*` files in `dotfiles/dotfiles/git/.config/git` directory
```
[user]
name = "<NAME>"
email = <EMAIL>
signingkey = <SIGNING KEY>
```
where the signing key is the GPG private key that can obtained
```
gpg --list-secret-keys --keyid-format LONG <EMAIL>
```
and the public key generated using
```
gpg --armor --export <SIGNING KEY>
```

### Set up wallpaper
enable and start `cronie`
```
sudo systemctl enable cronie
sudo systemctl start cronie
```
setup cron job by `crontab -e` and add these two lines
```
0 22 * * * <CONFIG HOME>/scripts/wallpaper-change.sh
0 6 * * * <CONFIG HOME>/scripts/wallpaper-change.sh
```
change variable `config_home` in `<CONFIG HOME>/scripts/wallpaper-change.sh`

### Set up lock


### Set up xournalpp
- enable `xournal-keys-master` plugin in `Plugin > Plugin Manager`
- change toolbars to `dracula` in `View > Toolbars > Dracula`
- link palete of colors by running this in `$XDG_CONFIG_HOME/xournalpp/` directory
```
ln -sf <.gpl TEMPLATE> palette.gpl
```

### TLP and battery for lenovo ntb
Battery charging threshold
- https://wiki.archlinux.org/title/Laptop/Lenovo
- https://www.reddit.com/r/archlinux/comments/gqbzkh/how_to_limit_battery_charging/
- https://wiki.archlinux.org/title/TLP
- https://chatgpt.com/share/678251ab-cf00-800b-a42a-3568690d36f2
- https://www.reddit.com/r/Fedora/comments/qpaa4g/tlp_vs_powerprofilesdaemon/

### Monitor
`xrandr --output DisplayPort-X --pos 0x0 --scale 1.8x1.8 --output eDP --pos {1920 * 1.8}x0`
